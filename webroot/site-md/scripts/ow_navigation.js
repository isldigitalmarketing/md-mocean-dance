// --------------------------------------------------------------------------------
// ow_navigation.js
// unknown author (possibly Matthew Hogg?) - code added to OneWeb build 2007-04-18
// Drives the dropdown navigation for the front-end of a site.
// --------------------------------------------------------------------------------
var DDNav;

function DDNavigation(NodeTag) {
	
	this.NodeTag = NodeTag.split(",");
	this.NodeClass = "md_dd_node";
	this.Opened = "";
	this.Closing = true;
	this.Selects;
	
	this.GetElementsByClass = function(cls, tag) {
		var arr = new Array();
		var pattern = "(^" + cls + "$|\\s" + cls + "\\s|\\s" + cls + "\\s|^" + cls + "\\s|\\s" + cls + "$)";
		var j = 0;
		var i;
		var elm = document.getElementsByTagName(tag.toLowerCase());
		for (i = 0; i < elm.length; i++) {
			var rex = new RegExp(pattern, "gi");
			if (rex.test(elm[i].className)) arr[arr.length] = elm[i];
		}
		return arr;
	}
	
	this.SetEvent = function(elm, ev, fn) {
		if (elm.addEventListener) elm.addEventListener(ev, fn, false);
		else if (elm.attachEvent) elm.attachEvent("on" + ev, fn);
		else elm.setAttribute("on" + ev, fn);
	}
	
	this.GetStyle = function(elm, prop) {
		if (window.getComputedStyle) {
			return window.getComputedStyle(elm, null).getPropertyValue(prop);
		} else if (elm.currentStyle) {
			var ieProp = "";
			if (prop.indexOf("-") != -1) {
				for (var i = 0; i < prop.length; i++) {
					if (prop.charAt(i) == "-") {
						i++;
						ieProp += prop.charAt(i).toUpperCase();
					} else {
						ieProp += prop.charAt(i);
					}
				}
			} else {
				ieProp = prop;
			}
			return eval("elm.currentStyle." + ieProp);
		}
	}
	
	this.GetOffset = function(elm, dir) {
		var n = 0;
		var item = eval("elm");
		var off = "offsetTop";
		var padding = "padding-top";
		if (dir == "x") {
			off = "offsetLeft";
			padding = "padding-left";
		}
		do {
			n += eval("item." + off);
			item = eval("item.offsetParent");
		} while (item != null);
		return n;
	}
	
	this.PositionY = function(elm) { var y = DDNav.GetOffset(elm, "y"); y += elm.offsetHeight; return y; }

	this.PositionX = function(elm) { var x = DDNav.GetOffset(elm, "x"); return x; }
	
	this.Overlapping = function(a, b) {
		var ax = DDNav.GetOffset(a, "x");
		var ay = DDNav.GetOffset(a, "y");
		var aw = a.offsetWidth;
		var ah = a.offsetHeight;
		var bx = DDNav.GetOffset(b, "x");
		var by = DDNav.GetOffset(b, "y");
		var bw = b.offsetWidth;
		var bh = b.offsetHeight;
    if (((ax + aw) < bx) || (ax > (bx + bw)) || ((ay + ah) < by) || (ay > (by + bh))) return false;
    else return true;
	}
	
	this.CheckTag = function(t) {
		var found = false;
		for (var i = DDNav.NodeTag.length - 1; i >= 0; i--) {
			if (t.toLowerCase() == DDNav.NodeTag[i].toLowerCase()) {
				found = true;
				break;
			}
		}
		return found;
	}
	
	this.FixSelects = function() { if (document.all) for (var i = DDNav.Selects.length - 1; i >= 0; i--) if (DDNav.Selects[i].style.visibility == "hidden") DDNav.Selects[i].style.visibility = "visible"; }
	
	this.HuntMenu = function() { DDNav.Closing = true; setTimeout("DDNav.KillMenu('" + DDNav.Opened + "')", 550); }
	
	this.KillMenu = function(m) { if (DDNav.Closing && document.getElementById(m)) DDNav.CloseMenu(m); }
	
	this.CloseMenu = function(m) {
		var n = (m).replace("_menu", "");
		var menu = document.getElementById(m);
		var node = document.getElementById(n);
		node.className = (node.className).replace(" " + DDNav.NodeClass + "_on", "");
		menu.style.display = "none";
		DDNav.FixSelects();
	}
	
	this.MaintainMenu = function() { DDNav.Closing = false; }

	this.ShowMenu = function(e) {
		if (!e) var e = window.event;
		if (e.target) var elm = e.target;
		else var elm = e.srcElement;
		if (elm.nodeType == 3) elm = elm.parentNode; // defeat Safari bug
		while (!DDNav.CheckTag(elm.tagName)) elm = elm.parentNode;
		if (DDNav.Opened != "") DDNav.CloseMenu(DDNav.Opened);
		var menu = document.getElementById(elm.id + "_menu");
		if (menu) {
			if (menu.childNodes.length > 0) {
				elm.className = (elm.className).replace(" " + DDNav.NodeClass + "_on", "");
				elm.className = elm.className + " " + DDNav.NodeClass + "_on";
				var x = DDNav.PositionX(elm);
				var y = DDNav.PositionY(elm);
				menu.style.left = x + "px"
				menu.style.top = y + "px";
				menu.style.display = "block";
				if (document.all) for (var i = DDNav.Selects.length - 1; i >= 0; i--)	if (DDNav.Overlapping(menu, DDNav.Selects[i])) DDNav.Selects[i].style.visibility = "hidden";
				if (DDNav.GetStyle(menu, "display") != "none") {
					DDNav.SetEvent(menu, "mouseover", DDNav.MaintainMenu);
					DDNav.SetEvent(menu, "mouseout", DDNav.HuntMenu);
					DDNav.Opened = menu.id;
				} else DDNav.Opened = "";
			}
		} else DDNav.Opened = "";
		DDNav.Closing = false;
	}
	
	// INITIALIZATION.	
	
	if (document.getElementById) {
		var ieMac = (document.all && navigator.userAgent.indexOf("Mac") != -1);
		if (!ieMac) {
			var node = new Array();
			for (var i = this.NodeTag.length - 1; i >= 0; i--) {
				var arr = this.GetElementsByClass(this.NodeClass, this.NodeTag[i]);
				for (var j = arr.length - 1; j >= 0; j--) node.push(arr[j]);
			}
			for (var i = node.length - 1; i >= 0; i--) {
				this.SetEvent(node[i], "mouseover", this.ShowMenu);
				this.SetEvent(node[i], "mouseout", this.HuntMenu);
			}
			if (document.all) this.Selects = document.getElementsByTagName("select");
		}
	}
}

ow_f_AppendInitEvent(ow_NavInit);

function ow_NavInit() { DDNav = new DDNavigation("a"); }
