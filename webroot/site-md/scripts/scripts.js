$(document).ready(function () {
	if (typeof OneWeb.Admin === "undefined" || typeof OneWeb.Admin.Preview != "undefined") {
		$('.flexslider').flexslider({
			animation: "slide",
			slideshowSpeed: 8000
		});
	} 
	else {
		$(".slides").css("height", "auto");
		$(".slide").css("margin-bottom", "15px");
}
});