<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" omit-xml-declaration="yes" indent="no" />
  <xsl:include href="../../core/templates/NavCore.xslt"/>

<!-- input parameters -->
<xsl:param name="currentID"/>
<xsl:param name="domainName"/>
<xsl:param name="secureDomainName"/>
<xsl:param name="sitePath"/>

<xsl:template match="/Pages">
	<xsl:call-template name="crumb">
		<xsl:with-param name="pageID" select="$currentID"/>
    <xsl:with-param name="endPageID" select="$currentID"/>
	</xsl:call-template>
</xsl:template>
 
<xsl:template name="crumb">
	<xsl:param name="pageID"/>
  <xsl:param name="endPageID"/>
	<xsl:variable name="page" select="/Pages/Page[@PageId=$pageID]"/>
  <xsl:variable name="endPage" select="/Pages/Page[@PageId=$endPageID]"/>
	<xsl:if test="$page/@ParentId!=0">
		<!-- recursively call parent crumb first -->
		<xsl:call-template name="crumb">
			<xsl:with-param name="pageID" select="$page/@ParentId"/>
      <xsl:with-param name="endPageID" select="$endPage/@PageId"/>
		</xsl:call-template>
    <span class="sitepath_div"><xsl:text disable-output-escaping="yes"> &gt; </xsl:text></span>
	</xsl:if>
  <xsl:choose>
    <xsl:when test="$page/@PageId=$endPage/@PageId">
      <xsl:value-of select="$page/@NavText"/>
    </xsl:when>
    <xsl:otherwise>
      <a>
		  <xsl:call-template name="formatAddress">
			  <xsl:with-param name="page" select="$page"/>
		  </xsl:call-template>
        <xsl:value-of select="$page/@NavText"/>
      </a>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>
 
 </xsl:stylesheet>
