<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html" omit-xml-declaration="yes" indent="yes" />
  <xsl:include href="../../core/templates/NavCore.xslt"/>

<xsl:param name="currentID"/>
<xsl:param name="domainName"/>
<xsl:param name="secureDomainName"/>
<xsl:param name="sitePath"/>

<!-- determine the root page id as the first page in display order not hidden -->
<xsl:variable name="tier1Id"><xsl:call-template name="getHomePageId"/></xsl:variable>

<xsl:template match="/Pages">
   <xsl:apply-templates select="Page[@TierLevel=2 and @ParentId=$tier1Id and not(contains(@Hidden, $navigation) or contains(@HiddenByParent,$navigation) or @Locked='true' or @LockedByParent='true')]"/>
</xsl:template>

<xsl:template match="Page">
	<xsl:variable name="pageId" select="@PageId"/>
	<xsl:if test="parent::*/Page[@ParentId=$pageId and not(contains(@Hidden, $navigation) or contains(@HiddenByParent,$navigation) or @Locked='true' or @LockedByParent='true')]">
		<div class="md_dd_menu">
		<xsl:attribute name="id"><xsl:value-of select="concat('ow_mainNav_',$pageId, '_menu')"/></xsl:attribute>
		<ul>
		<xsl:apply-templates select="parent::*/Page[@TierLevel=3 and @ParentId=$pageId and not(contains(@Hidden, $navigation) or contains(@HiddenByParent,$navigation) or @Locked='true' or @LockedByParent='true')]"/>
		</ul></div>
	</xsl:if>
</xsl:template>

<xsl:template match="Page[@TierLevel=3]">
	<li><a>
    <xsl:call-template name="formatAddress"/>
		<xsl:value-of select="@NavText"/>
	</a>
	</li>
</xsl:template>

</xsl:stylesheet>
