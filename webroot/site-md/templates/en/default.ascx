<%@ Control Language="vb" AutoEventWireup="false" Inherits="ISL.OneWeb4.UI.Components.Templates.Template" FormControl="~/core/controls/en/Form.ascx" %>
<%@ Register TagPrefix="OW" Namespace="ISL.OneWeb4.UI.Components.Templates" Assembly="ISL.OneWeb4.UI.Web" %>
<%@ Register TagPrefix="OWWeb" Assembly="ISL.OneWeb4.UI.Controls" Namespace="ISL.OneWeb4.UI.WebControls" %>
<%@ Register TagPrefix="OWSite" Assembly="ISL.OneWeb4.UI.Web" Namespace="ISL.OneWeb4.UI.WebControls" %>
<%@ Register TagPrefix="OWSite" TagName="Menu" Src="~/core/controls/Menu.ascx" %>
<%@ Register TagPrefix="OWSite" TagName="SearchBox" Src="~/core/controls/en/SearchBox.ascx" %>
<%@ Register TagPrefix="OW" TagName="IncludejQuery" Src="~/core/controls/IncludejQuery.ascx" %>

<%-- custom editor CSS --%>
<OWWeb:Stylesheet Runat="server" Media="screen" Mode="Link" Url="~/site-md/styles/ow_custom.css" EnableViewState="false"/>
<OW:IncludejQuery runat="server" IncludeUI="true" />

<div id="container">
	<div class="mh">
		<table class="mh_table" cellpadding="0">
			<tr>
				<td class="mh_globallinks"><%-- contact us --%><OWSite:Hyperlink runat="server" NavigatePage="8,en" Text="contact us" /><%-- print --%><OWSite:Printlink runat="server" Text="print this page" /><%-- search --%><OWSite:Hyperlink runat="server" NavigatePage="3,en" Text="search" /></td>
				<td class="mh_logo" rowspan="2"><%-- masthead --%><OWSite:Hyperlink runat="server" NavigateUrl="~~/"><img src="/site-md/images/mh_logo.png" width="176" height="112" alt="mocean dance" hspace="0" vspace="0" border="0" /></OWSite:Hyperlink></td>
			</tr>
			<tr>
				<td class="mh_menu"><%-- main menu --%><OWSite:Menu runat="server" TransformSource="~/site-md/templates/MainMenu.xslt"/></td>
			</tr>
		</table>
	</div>
	<div class="ins_container">
	<!--  Home Page Slider Start  -->
		<div class="flexslider">
			<ul class="slides">
				<OW:DependentBlock runat="server" DependsOnPosition="slide_1_image">
					<li class="slide slide_1">
						<div class="clearfix slide-image">
				</OW:DependentBlock>

							<OW:ContentBlock Position="slide_1_image" Display="Block" CssClass="ow_block" runat="server" />

				<OW:DependentBlock runat="server" DependsOnPosition="slide_1_image">
						</div>
					</li>
				</OW:DependentBlock>

				<OW:DependentBlock runat="server" DependsOnPosition="slide_2_image">
					<li class="slide slide_2">
						<div class="clearfix slide-image">
				</OW:DependentBlock>

							<OW:ContentBlock Position="slide_2_image" Display="Block" CssClass="ow_block" runat="server" />

				<OW:DependentBlock runat="server" DependsOnPosition="slide_2_image">
						</div>
					</li>
				</OW:DependentBlock>

				<OW:DependentBlock runat="server" DependsOnPosition="slide_3_image">
					<li class="slide slide_3">
						<div class="clearfix slide-image">
				</OW:DependentBlock>

							<OW:ContentBlock Position="slide_3_image" Display="Block" CssClass="ow_block" runat="server" />

				<OW:DependentBlock runat="server" DependsOnPosition="slide_3_image">
						</div>
					</li>
				</OW:DependentBlock>

				<OW:DependentBlock runat="server" DependsOnPosition="slide_4_image">
					<li class="slide slide_4">
						<div class="clearfix slide-image">
				</OW:DependentBlock>

							<OW:ContentBlock Position="slide_4_image" Display="Block" CssClass="ow_block" runat="server" />

				<OW:DependentBlock runat="server" DependsOnPosition="slide_4_image">
						</div>
					</li>
				</OW:DependentBlock>

				<OW:DependentBlock runat="server" DependsOnPosition="slide_5_image">
					<li class="slide slide_5">
						<div class="clearfix slide-image">
				</OW:DependentBlock>

							<OW:ContentBlock Position="slide_5_image" Display="Block" CssClass="ow_block" runat="server" />

				<OW:DependentBlock runat="server" DependsOnPosition="slide_5_image">
						</div>
					</li>
				</OW:DependentBlock>

				<OW:DependentBlock runat="server" DependsOnPosition="slide_6_image">
					<li class="slide slide_6">
						<div class="clearfix slide-image">
				</OW:DependentBlock>

							<OW:ContentBlock Position="slide_6_image" Display="Block" CssClass="ow_block" runat="server" />

				<OW:DependentBlock runat="server" DependsOnPosition="slide_6_image">
						</div>
					</li>
				</OW:DependentBlock>
			</ul>
		</div>

	<table class="home_content_table" cellpadding="0">
		<tr>
			<td class="home_content_lc"><%-- main left cb --%><OW:ContentBlock Position="main_lc" Display="Block" CssClass="ow_block" runat="server" /></td>
			<td class="home_content_rc"><%-- main right cb --%><OW:ContentBlock Position="main_rc" Display="Block" CssClass="ow_block" runat="server" /></td>
		</tr>
	</table>
	<div class="footer"><table class="footer_table" cellpadding="0">
		<tr>
			<td class="footer_lc"><%-- footer left cb --%><OW:ContentBlock Position="footer_lc" Display="Block" CssClass="ow_block" runat="server" /></td>
			<td class="footer_rc"><%-- footer right cb --%><OW:ContentBlock Position="footer_rc" Display="Block" LimitedAccess="True" CssClass="ow_block" runat="server" /></td>
		</tr>
	</table></div>
	</div>
	<div class="photocredit">Photograph by Holly Crooks</div>
</div>

<script type="text/javascript" src="/site-md/scripts/jquery.flexslider.js"></script>
<script type="text/javascript" src="/site-md/scripts/scripts.js"></script>

<%-- dropdown menus --%>
<OWWeb:ScriptBlock runat="server" ScriptType="text/javascript" DocumentSource="~/site-md/scripts/ow_navigation.js" EnableViewState="false" />
<OWSite:Menu runat="server" TransformSource="~/site-md/templates/MainMenuSub.xslt"/>