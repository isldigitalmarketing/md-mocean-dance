<%@ Control Language="vb" AutoEventWireup="false" Inherits="ISL.OneWeb4.UI.Components.Templates.Template" FormControl="~/core/controls/en/Form.ascx" %>
<%@ Register TagPrefix="OW" Namespace="ISL.OneWeb4.UI.Components.Templates" Assembly="ISL.OneWeb4.UI.Web" %>
<%@ Register TagPrefix="OWWeb" Assembly="ISL.OneWeb4.UI.Controls" Namespace="ISL.OneWeb4.UI.WebControls" %>
<%@ Register TagPrefix="OWSite" Assembly="ISL.OneWeb4.UI.Web" Namespace="ISL.OneWeb4.UI.WebControls" %>
<%@ Register TagPrefix="OWSite" TagName="Menu" Src="~/core/controls/Menu.ascx" %>
<%@ Register TagPrefix="OWSite" TagName="SearchBox" Src="~/core/controls/en/SearchBox.ascx" %>
<%@ Register TagPrefix="OWSite" TagName="Sitemap" Src="~/core/controls/en/SitemapResults.ascx" %>

<%-- custom editor CSS --%>
<OWWeb:Stylesheet Runat="server" Media="screen" Mode="Link" Url="/site-md/styles/ow_custom.css" EnableViewState="false" />

<OWWeb:Stylesheet Runat="server" Media="screen" Mode="Link" Url="/site-md/styles/inside.css" EnableViewState="false" />

<div id="container">
	<div class="mh">
		<table class="mh_table" cellpadding="0">
			<tr>
				<td class="mh_globallinks"><%-- home link --%><OWSite:Hyperlink runat="server" NavigateUrl="~~/" Text="home" /><%-- contact us --%><OWSite:Hyperlink runat="server" NavigatePage="8,en" Text="contact us" /><%-- print --%><OWSite:Printlink runat="server" Text="print this page" /><%-- search --%><OWSite:Hyperlink runat="server" NavigatePage="3,en" Text="search" /></td>
				<td class="mh_logo" rowspan="2"><%-- masthead --%><OWSite:Hyperlink runat="server" NavigateUrl="~~/"><img src="/site-md/images/mh_logo.png" width="176" height="112" alt="mocean dance" hspace="0" vspace="0" border="0" /></OWSite:Hyperlink></td>
			</tr>
			<tr>
				<td class="mh_menu"><%-- main menu --%><OWSite:Menu runat="server" TransformSource="~/site-md/templates/MainMenu.xslt"/></td>
			</tr>
		</table>
	</div>
	<div class="ins_container"><table class="inside_fullspan_table" cellpadding="0">
		<tr>
			<td class="fullspan"><div class="fs_sitepath"></div>
			<div class="fs_pagetitle"><%-- page title --%><OWSite:Menu runat="server" TransformSource="~/core/templates/CurrentHeader.xslt" ContentSEO="False" /></div>
			<div class="fs_cblock1"><%-- site map --%><OWSite:Sitemap runat="server" TransformSource="~/core/templates/SitemapResults.xslt" /></div></td>
		</tr>
	</table>
	<div class="footer"><table class="footer_table" cellpadding="0">
		<tr>
			<td class="footer_lc"><%-- footer left cb --%><OW:ContentBlock Position="footer_lc" Display="Block" CssClass="ow_block" runat="server" /></td>
			<td class="footer_rc"><%-- footer right cb --%><OW:ContentBlock Position="footer_rc" Display="Block" LimitedAccess="True" CssClass="ow_block" runat="server" /></td>
		</tr>
	</table></div>
	</div>
	<div class="photocredit">Photograph by Holly Crooks</div>
</div>
<%-- dropdown menus --%>
<OWWeb:ScriptBlock runat="server" ScriptType="text/javascript" DocumentSource="~/site-md/scripts/ow_navigation.js" EnableViewState="false" />
<OWSite:Menu runat="server" TransformSource="~/site-md/templates/MainMenuSub.xslt"/>