<%@ Control Language="vb" AutoEventWireup="false" Inherits="ISL.OneWeb4.UI.Components.Templates.Template" FormControl="~/core/controls/en/Form.ascx" %>
<%@ Register TagPrefix="OW" Namespace="ISL.OneWeb4.UI.Components.Templates" Assembly="ISL.OneWeb4.UI.Web" %>
<%@ Register TagPrefix="OWSite" Assembly="ISL.OneWeb4.UI.Web" Namespace="ISL.OneWeb4.UI.WebControls" %>
<%@ Register TagPrefix="OWSite" TagName="Sitemap" Src="~/core/controls/en/SitemapResults.ascx" %>
<%@ Register TagPrefix="OWSite" TagName="Menu" Src="~/core/controls/Menu.ascx" %>

<%-- logo --%>
<div id="printlogo"><OWSite:Hyperlink runat="server" NavigateUrl="~~/"><img src="/site-md/images/mh_logo_pr.jpg" width="241" height="84" alt="mocean dance" hspace="0" vspace="0" border="0" /></OWSite:Hyperlink></div>

<%-- content --%>
<h1><OWSite:Menu runat="server" TransformSource="~/core/templates/CurrentHeader.xslt" ContentSEO="False" /></h1>
<OWSite:Sitemap runat="server" TransformSource="~/core/templates/SitemapResults.xslt" />

<%-- footer --%>
<div id="printfooter">
	<%-- footer left cb --%><OW:ContentBlock Position="footer_lc" Display="Block" CssClass="ow_block" runat="server" />
	<br />
	<%-- footer right cb --%><OW:ContentBlock Position="footer_rc" Display="Block" LimitedAccess="True" CssClass="ow_block" runat="server" />
</div>